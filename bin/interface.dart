abstract class Animal {
  void typeName() {}
  void typeAnimal() {}
  void walk() {}
  void eat() {}
  void sleep() {}
}

class Lion implements Animal {
  void typeName() {
    print("Lion");
  }

  void typeAnimal() {
    print("Carnivora");
  }

  void walk() {
    print("Can Walk");
  }

  void eat() {
    print("Can Eat");
  }

  void sleep() {
    print("Can Sleep");
  }
}

void main() {
  // Creating Object of the implementation class
  Lion lion = new Lion();
  lion.typeName();
  lion.typeAnimal();
  lion.walk();
  lion.eat();
  lion.sleep();
}
